layui.use(['form','layer','layedit','laydate','upload'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laypage = layui.laypage,
        upload = layui.upload,
        layedit = layui.layedit,
        laydate = layui.laydate,
        $ = layui.jquery;

    //用于同步编辑器内容到textarea
    // layedit.sync(editIndex);

    //表单提交-新增
    form.on("submit(add-btn)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        // 实际使用时的提交信息
        setTimeout(function(){
            $.ajax({
                url: addUrl,
                data: data.field,
                type: "POST",
                dataType: "json",
                success: function (res) {
                    if (res.code == 0) {
                        top.layer.close(index);
                        top.layer.msg(res.message);
                        layer.closeAll("iframe");
                        //刷新父页面
                        parent.location.reload();
                    } else {
                        layer.msg(res.message, {icon: 5}); 
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        },500); 
    })

    //表单提交-编辑
    form.on("submit(upd-btn)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        // 实际使用时的提交信息
        setTimeout(function () {
            $.ajax({
                url: updUrl,
                data: data.field,
                type: "POST",
                dataType: "json",
                success: function (res) {
                    if (res.code == 0) {
                        top.layer.close(index);
                        top.layer.msg(res.message);
                        layer.closeAll("iframe");
                        //刷新父页面
                        parent.location.reload();
                    } else {
                        layer.msg(res.message, {icon: 5}); 
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }, 500); 
    })


})