layui.use(['form','layer','laydate','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        table = layui.table;

    //角色列表
    var tableIns = table.render({
        elem: '#role-list',
        url : dataUrl,
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limit : 20,
        limits : [10,15,20,25],
        id : "role-list-table",
        cols : [[
            {field: 'rid', title: 'ID', width:60,sort:true, align:"center"},
            {field: 'rname', title: '角色名',sort:true,align:'center'},
            {field: 'remake', title: '备注',sort:true, align:'center'},
            {title: '操作', width:200,fixed:"right",align:"center",templet:function(d){
                var dom = '<a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>';
                    dom +='<a class="layui-btn layui-btn-xs layui-btn-danger"  lay-event="del">删除</a>';
                    dom +='<a class="layui-btn layui-btn-xs layui-btn-primary"  lay-event="auth">授权</a>';
                return dom;
            }}
        ]]
    });


    //排序
    table.on('sort(manage-list)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
      table.reload('manage-list-table', {
        initSort: obj, //记录初始排序，如果不设的话，将无法标记表头的排序状态。
        where: { 
          order_field: obj.field, //排序字段   
          order_type: obj.type, //排序方式   
          article_name: $(".searchVal").val()  //搜索的关键字
        }
      });
    });

    //添加角色
    $(".add-manage-btn").click(function(){
        opentWindow('添加角色',addUrl,0);
    })

    //打开窗口
    function opentWindow(title,contentUrl,rid){
        var index = layui.layer.open({
            title : title,
            type : 2,
            area: ['600px', '400px'],
            content : contentUrl+'?rid='+rid,
            maxmin: true, // 显示最大化
            success : function(layero, index){
                setTimeout(function(){
                    layui.layer.tips('点击此处返回角色列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
    }



    //列表操作
    table.on('tool(role-list)', function(obj){
        var layEvent = obj.event,data = obj.data;
        if(layEvent === 'edit'){ //编辑
            opentWindow('编辑角色',updUrl,data.rid);
        } else if(layEvent === 'del'){ //删除
            del(data);
        } else if(layEvent === 'auth'){ //授权
            opentWindow('授权',authUrl,data.rid);
        }
    });

    //删除
    function del(data) {
        layer.confirm('确定删除此角色？', { icon: 3, title: '提示信息' }, function (index) {
            var index = layer.msg('操作中，请稍候', { icon: 16, time: false, shade: 0.8 });
            setTimeout(function () {
                $.ajax({
                    url: delUrl,
                    data: {
                        'rid': data.rid,
                    },
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        layer.msg(res.message);
                        layer.close(index);
                        tableIns.reload();
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }, 500);
        });
    }
})