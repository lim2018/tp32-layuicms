layui.use(['form', 'layer'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laypage = layui.laypage,
        $ = layui.jquery;

    //表单提交
    form.on("submit(upd-btn)", function (data) {
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', { icon: 16, time: false, shade: 0.8 });
        // 实际使用时的提交信息
        setTimeout(function () {
            $.ajax({
                url: post_url,
                data: $('#form').serialize(),
                type: "POST",
                dataType: "json",
                success: function (res) {
                    if (res.code == 0) {
                        layer.msg(res.message);
                        location.reload();//刷新页面
                    } else {
                        layer.msg(res.message, { icon: 5 });
                    }
                }
            });
        }, 500);
    })

})