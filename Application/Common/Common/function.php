<?php
/**公用函数库*/

/**
 * 返回Json数据
 * @param int $code
 * @param string $message
 * @param array $data
 * @return string
 */
function retJson($code,$message = '',$data = array())
{
    if(!is_numeric($code))
    {
        return '';
    }

    $result=array(
        'code' => $code,
        'message' => $message,
        'data' => $data
    );

    echo json_encode($result);
    exit;
}

/**
 * 返回Json数据-layui格式
 * @param int $code
 * @param string $message
 * @param array $data
 * @return string
 */
function retJsonLay($code,$msg = '',$count,$data = array())
{
    if(!is_numeric($code))
    {
        return '';
    }

    $result=array(
        'code' => $code,
        'msg' => $msg,
        'count' => $count,
        'data' => $data
    );

    echo json_encode($result);
    exit;
}


// js控制台输出
function jsConsoleLog($data) {
    if (is_array($data)) {
        echo '<script>';
        echo 'console.log(' . json_encode($data) . ');'; // 使用 console.log() 打印数组
        echo '</script>';
    } else {
        echo '<script>';
        echo 'console.log("' . $data . '");'; // 使用 console.log() 打印普通变量
        echo '</script>';
    }
}

/*
 * CMD控制台打印调试信息，而不在浏览器输出
 * param fixed $data    参数可以是除了对象以外的所有数据类型，比如：字符串，数组，jason等
 */
function phpconsole($data){
    $stdout = fopen('php://stdout', 'w');
    fwrite($stdout,json_encode($data, JSON_UNESCAPED_UNICODE)."\n");//JSON_UNESCAPED_UNICODE 可以解决汉字乱码问题
    fclose($stdout);
}

/*删除文件夹*/
function del_dir($dir) {
    $dh=opendir($dir);
    while ($file=readdir($dh)) {
        if($file!="." && $file!="..") {
            $fullpath=$dir."/".$file;
            if(!is_dir($fullpath)) {
                @unlink($fullpath);
            } else {
                del_dir($fullpath);
            }
        }
    }
    closedir($dh);
    if(rmdir($dir)) {
        return true;
    } else {
        return false;
    }
}

// 用角色权限id取对应url
function getHrefs($menuArray, $idString) {
    $idArray = explode(",", $idString);
    $hrefArray = [];

    function findHref($menuArray, $idArray, &$hrefArray) {
        foreach ($menuArray as $menu) {
            if (in_array($menu['id'], $idArray)) {
                $hrefArray[] = $menu['href'];
            }
            if (isset($menu['children'])) {
                findHref($menu['children'], $idArray, $hrefArray);
            }
        }
    }

    findHref($menuArray, $idArray, $hrefArray);

    return implode(",", $hrefArray);
}

// 过滤没权限菜单
function filterMenuArray($menuArray, $idString) {
    $idArray = explode(",", $idString);
    $filteredArray = [];

    foreach ($menuArray as $menu) {
        if (in_array($menu['id'], $idArray)) {
            $filteredMenu = $menu;
            if (isset($menu['children'])) {
                $filteredMenu['children'] = filterMenuArray($menu['children'], $idString);
            }
            $filteredArray[] = $filteredMenu;
        }
    }

    return $filteredArray;
}

