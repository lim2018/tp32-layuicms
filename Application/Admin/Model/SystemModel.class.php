<?php

namespace Admin\Model;

use Think\Model;

class SystemModel extends Model
{
	// 模型字段
	protected $fields = array(
		"id",				// 主键
		"name",				// 配置项名
		"value",			// 配置项值
		"remark"			// 备注
	);

	// 主键
	protected $pk     = 'id';

	// 取出站点数据
	public function getSystemData()
	{
		$data = array();
		$data["site_name"]       = $this->where(array("name" => "site_name"))->find()["value"];
		$data["seo_keyworld"]    = $this->where(array("name" => "seo_keyworld"))->find()["value"];
		$data["seo_des"]         = $this->where(array("name" => "seo_des"))->find()["value"];
		$data["copyright"]       = $this->where(array("name" => "copyright"))->find()["value"];
		$data["record"]          = $this->where(array("name" => "record"))->find()["value"];
		$data["statistics_code"] = $this->where(array("name" => "statistics_code"))->find()["value"];
		return $data;
	}

	// 更新信息
	public function updSystemData($data)
	{
		try {
			$this->where(array("name" => "site_name"))->setField("value", $data["site_name"]);
			$this->where(array("name" => "seo_keyworld"))->setField("value", $data["seo_keyworld"]);
			$this->where(array("name" => "seo_des"))->setField("value", $data["seo_des"]);
			$this->where(array("name" => "copyright"))->setField("value", $data["copyright"]);
			$this->where(array("name" => "record"))->setField("value", $data["record"]);
			$this->where(array("name" => "statistics_code"))->setField("value", $data["statistics_code"]);
			return true;
		} catch (\Throwable $th) {
			return false;
		}
	}
}
