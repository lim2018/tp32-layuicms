<?php

namespace Admin\Model;

use Think\Model;
use Admin\Model\ManageModel;

class RoleModel extends Model
{
    // 模型字段
    protected $fields = array(
        "rid",                        // 角色id
        "rname",                    // 角色名
        "auth",                        // 权限
        "is_del",                   // 删除：0正常，1被删除
        "remake",                   // 备注
    );

    // 主键
    protected $pk     = 'rid';

    // 查询
    public function selList($page, $limit, $order_field, $order_type)
    {
        $where = array(
            'is_del' => 0
        );
        $order = array(
            'rid' => 'desc'
        );
        if (!empty($order_field) && $order_type) {
            $order["$order_field"] = $order_type;
        }
        $count = $this->where($where)->count();
        $list  = $this->where($where)->order($order)->limit(($page - 1) * $limit . ',' . $limit)->select();
        return array(
            'code' => 0,
            'msg' => '',
            'count' => $count,
            'data' => $list
        );
    }

    // 添加
    public function addRoleData($data)
    {
        $row_num = $this->add($data);
        return $row_num > 0 ? true : false;
    }

    // 按名字查找
    public function getRoleByName($rname){
        $data = $this->where("rname",$rname)->find();
        return $data;
    }

    // 删除
    public function delRoleById($rid){
        $row_num = $this->where("rid = $rid")->setField("is_del",1);
        return $row_num > 0 ? true : false;
    }

    // 编辑
    public function updRoleData($data){
        $row_num = $this->save($data);
        return $row_num >= 0 ? true : false;
    }

    // 按id查找
    public function getRoleById($rid){
        $role = $this->find($rid);
        return $role;
    }

    // 更新权限
    public function updAuthById($rid,$ids){
        $row_num = $this->where("rid = $rid")->setField("auth",$ids);
        return $row_num >= 0 ? true : false;
    }

    // 按用户id取auth中ids
    public function getAuthById($uid){
        // $manage_user = (new ManageModel)->selById($uid);
    }
}
