<?php
return array(

    // 后台导航菜单
    'NAV_LIST' => array(
        array(
            "id"     => 100,                    // 编号
            "title"  => "系统设置",              // 名称
            "icon"   => "&#xe716;",             // 图标
            "href"   => "System/index",         // 链接
            "spread" => false,                  // 展开
            "is_show" => true                    // 显示
        ),
        array(
            "id"     => 200,                    // 编号
            "title"  => "内容管理",              // 名称
            "icon"   => "icon-text",             // 图标
            "href"   => "",         // 链接
            "spread" => false,                  // 展开
            "is_show" => true,                  // 显示
            "children" => array(
                array(
                    "id"     => 201,                    // 编号
                    "title"  => "文章列表",              // 名称
                    "icon"   => "",            // 图标
                    "href"   => "Article/index",        // 链接
                    "spread" => false,                  // 展开
                    "is_show" => true,                   // 显示
                    "children" => array(
                        array(
                            "id"      => 20101,
                            "title"   => "置顶",
                            "icon"    => "",
                            "href"    => "Article/upTop",
                            "spread"  => false,
                            "is_show" => false
                        ),
                        array(
                            "id"      => 20102,
                            "title"   => "删除",
                            "icon"    => "",
                            "href"    => "Article/del",
                            "spread"  => false,
                            "is_show" => false
                        ),
                        array(
                            "id"      => 20103,
                            "title"   => "批量删除",
                            "icon"    => "",
                            "href"    => "Article/delAll",
                            "spread"  => false,
                            "is_show" => false
                        ),
                        array(
                            "id"      => 20104,
                            "title"   => "新增",
                            "icon"    => "",
                            "href"    => "Article/add",
                            "spread"  => false,
                            "is_show" => false
                        ),
                        array(
                            "id"      => 20105,
                            "title"   => "编辑",
                            "icon"    => "",
                            "href"    => "Article/upd",
                            "spread"  => false,
                            "is_show" => false
                        ),
                    )

                )
            )
        ),

        array(
            "id"     => 300,                    // 编号
            "title"  => "系统用户",              // 名称
            "icon"   => "&#xe612;",             // 图标
            "href"   => "",         // 链接
            "spread" => false,                  // 展开
            "is_show" => true,                  // 显示
            "children" => array(
                array(
                    "id"     => 301,
                    "title"  => "管理员",
                    "icon"   => "",
                    "href"   => "Manage/index",
                    "spread" => false,
                    "is_show" => true,
                    "children" => array(
                        array(
                            "id"     => 30101,
                            "title"  => "删除",
                            "icon"   => "",
                            "href"   => "Manage/del",
                            "spread" => false,
                            "is_show" => false,
                        ),
                        array(
                            "id"     => 30102,
                            "title"  => "批量删除",
                            "icon"   => "",
                            "href"   => "Manage/delAll",
                            "spread" => false,
                            "is_show" => false,
                        ),
                        array(
                            "id"     => 30103,
                            "title"  => "添加用户",
                            "icon"   => "",
                            "href"   => "Manage/add",
                            "spread" => false,
                            "is_show" => false,
                        ),
                        array(
                            "id"     => 30104,
                            "title"  => "编辑用户",
                            "icon"   => "",
                            "href"   => "Manage/upd",
                            "spread" => false,
                            "is_show" => false,
                        ),
                        array(
                            "id"     => 30105,
                            "title"  => "编辑用户",
                            "icon"   => "",
                            "href"   => "Manage/upd",
                            "spread" => false,
                            "is_show" => false,
                        ),
                        array(
                            "id"     => 30106,
                            "title"  => "重置密码",
                            "icon"   => "",
                            "href"   => "Manage/pass",
                            "spread" => false,
                            "is_show" => false,
                        ),
                    )
                ),
                array(
                    "id"     => 302,
                    "title"  => "角色分组",
                    "icon"   => "",
                    "href"   => "Role/index",
                    "spread" => false,
                    "is_show" => true,
                    "children" => array(
                        array(
                            "id"     => 30201,
                            "title"  => "删除",
                            "icon"   => "",
                            "href"   => "Role/del",
                            "spread" => false,
                            "is_show" => false,
                        ),
                        array(
                            "id"     => 30202,
                            "title"  => "添加角色",
                            "icon"   => "",
                            "href"   => "Role/add",
                            "spread" => false,
                            "is_show" => false,
                        ),
                        array(
                            "id"     => 30203,
                            "title"  => "编辑角色",
                            "icon"   => "",
                            "href"   => "Role/upd",
                            "spread" => false,
                            "is_show" => false,
                        ),
                        array(
                            "id"     => 30204,
                            "title"  => "分配权限",
                            "icon"   => "",
                            "href"   => "Role/auth",
                            "spread" => false,
                            "is_show" => false,
                        ),
                    )
                ),
            )
        ),

        array(
            "id"     => 400,                    // 编号
            "title"  => "基础数据",              // 名称
            "icon"   => "&#xe631;",             // 图标
            "href"   => "",                     // 链接
            "spread" => false,                  // 展开
            "is_show" => true,                   // 显示
            "children" => array(
                array(
                    "id"     => 401,
                    "title"  => "其他页面",
                    "icon"   => "",
                    "href"   => "#",
                    "spread" => false,
                    "is_show" => true
                ),
                array(
                    "id"     => 402,
                    "title"  => "删除页面",
                    "icon"   => "",
                    "href"   => "#",
                    "spread" => false,
                    "is_show" => false
                ),
            )
        )
    ),

    // 系统配置
    'APP_SYS_NAME' => 'TP32LAYUICMS',

);
