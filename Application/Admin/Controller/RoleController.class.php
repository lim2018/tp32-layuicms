<?php

namespace Admin\Controller;

use Admin\Model\RoleModel;

class RoleController extends BaseController
{

    private $role_md;

    public function __construct()
    {
        parent::__construct();
        $this->role_md = new RoleModel();
    }

    // 角色列表
    public function index()
    {
        if (IS_AJAX) {
            $page        = I('param.page');
            $limit       = I('param.limit');
            $order_field = I('param.order_field');
            $order_type  = I('param.order_type');
            $r = $this->role_md->selList($page, $limit, $order_field, $order_type);
            return retJsonLay($r['code'], $r['msg'], $r['count'], $r['data']);
        } else {
            return $this->display();
        }
    }

    // 添加角色 
    public function add()
    {
        if (IS_AJAX) {
            $rname = I("post.rname");
            if ($this->role_md->getRoleByName($rname)) {
                return retJson(2, "角色名已存在！");
            }
            $data = I("post.");
            $add_result = $this->role_md->addRoleData($data);
            if ($add_result) {
                return retJson(0, "添加成功");
            } else {
                return retJson(1, "添加失败！");
            }
        } else {
            return $this->display();
        }
    }

    // 删除角色
    public function del()
    {
        $rid = I("post.rid");
        $del_result = $this->role_md->delRoleById($rid);
        if ($del_result) {
            return retJson(0, "删除成功");
        } else {
            return retJson(1, "删除失败！");
        }
    }

    // 编辑角色
    public function upd()
    {
        if (IS_AJAX) {
            $data = I("post.");
            $upd_result = $this->role_md->updRoleData($data);
            if ($upd_result) {
                return retJson(0, "更新成功");
            } else {
                return retJson(1, "更新失败！");
            }
        } else {
            $rid = I("get.rid");
            $role = $this->role_md->getRoleById($rid);
            $this->assign("role", $role);
            return $this->display();
        }
    }

    // 授权
    public function auth(){
        if (IS_AJAX){
            $rid = I("post.rid");
            $ids = I("post.ids");
            $upd_result = $this->role_md->updAuthById($rid,$ids);
            if ($upd_result) {
                return retJson(0, "更新成功");
            } else {
                return retJson(1, "更新失败！");
            }
        } else {
            $json_nav = json_encode(C('NAV_LIST'));
            $role = $this->role_md->getRoleById(I('get.rid'));
            $this->assign("rid",I("get.rid"));
            $this->assign("json_nav",$json_nav);
            $this->assign("role",$role);
            return $this->display();
        }
    }
}
