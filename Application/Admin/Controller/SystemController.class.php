<?php
namespace Admin\Controller;
use Admin\Model\SystemModel;

class SystemController extends BaseController 
{
	// 站点设置
    public function index()
	{
		// 实例化模型
		$system_model = new SystemModel();
    	if (!IS_AJAX) { // 显示网页
			$sys_data = $system_model->getSystemData();
			$this->assign("sys_data",$sys_data);
			return $this->display();
		}

		// 处理数据提交
		$data = I("post.");
		if ( $system_model->updSystemData($data) ) {
			F('sys_info', null);
			return retJson(0,'更新成功','');
    	} else {
			return retJson(1,'未知原因，更新失败!','');
    	}

    }

    
}