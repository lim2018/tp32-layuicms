<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/Public/layuicms/layui/css/layui.css">
    <title>Document</title>
    <style>
        .logo{
            width: 100%;
            height: 40px;
            padding: 10px 30px;
        }
        .logo h2{
            font-weight: bold;
        }
    </style>
</head>

<body>
    <div class="logo">
        <h2>XXX博客园</h2>
    </div>
    <ul class="layui-nav" lay-filter="">
        <li class="layui-nav-item"><a href="">最新活动</a></li>
        <li class="layui-nav-item layui-this"><a href="">产品</a></li>
        <li class="layui-nav-item"><a href="">大数据</a></li>
        <li class="layui-nav-item">
            <a href="javascript:;">解决方案</a>
            <dl class="layui-nav-child">
                <!-- 二级菜单 -->
                <dd><a href="">移动模块</a></dd>
                <dd><a href="">后台模版</a></dd>
                <dd><a href="">电商平台</a></dd>
            </dl>
        </li>
        <li class="layui-nav-item"><a href="">社区</a></li>
    </ul>

    <script src="/Public/layuicms/layui/layui.all.js"></script>
</body>

</html>