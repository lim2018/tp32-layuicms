<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>系统设置</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="/Public/layuicms/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/Public/layuicms/css/public.css" media="all" />
	<style>
		.border-box {
			border: 1px solid #eee;
			padding: 10px 10px 10px 0
		}

		.disabled {
			background: #eee
		}
	</style>
</head>

<body class="childrenBody">
	<form class="layui-form layui-row layui-col-space10" id="form">
		<div class="layui-col-md9 layui-col-xs12 border-box">

			<div class="layui-form-item magt3">
				<label class="layui-form-label">站点名称</label>
				<div class="layui-input-block">
					<input type="text" class="layui-input" lay-verify="required" name="site_name" value="<?php echo ($sys_data["site_name"]); ?>"
						placeholder="（必填）请输入站点名称" >
				</div>
			</div>

			<div class="layui-form-item magt3">
				<label class="layui-form-label">SEO关键字</label>
				<div class="layui-input-block">
					<input type="text" class="layui-input"  name="seo_keyworld" value="<?php echo ($sys_data["seo_keyworld"]); ?>"
						placeholder="（可选）请输入关键字">
				</div>
			</div>

			<div class="layui-form-item magt3">
				<label class="layui-form-label">SEO描述</label>
				<div class="layui-input-block">
					<textarea name="seo_des" placeholder="（可选）请输入描述" class="layui-textarea"><?php echo ($sys_data["seo_des"]); ?></textarea>
				</div>
			</div>

			<div class="layui-form-item magt3">
				<label class="layui-form-label">版权信息</label>
				<div class="layui-input-block">
					<input type="text" class="layui-input"  name="copyright" value="<?php echo ($sys_data["copyright"]); ?>"
						placeholder="（可选）请输入版权信息">
				</div>
			</div>

			<div class="layui-form-item magt3">
				<label class="layui-form-label">备案号</label>
				<div class="layui-input-block">
					<input type="text" class="layui-input"  name="record" value="<?php echo ($sys_data["record"]); ?>"
					 placeholder="（可选）请输入备案号">
				</div>
			</div>

			<div class="layui-form-item magt3">
				<label class="layui-form-label">统计代码</label>
				<div class="layui-input-block">
					<textarea name="statistics_code" placeholder="（可选）请输入统计代码" class="layui-textarea"><?php echo ($sys_data["statistics_code"]); ?></textarea>
				</div>
			</div>

			<hr class="layui-bg-gray" />
			<div class="layui-right">
				<a class="layui-btn layui-btn-sm" lay-filter="upd-btn" lay-submit><i
						class="layui-icon">&#xe609;</i>提交</a>
			</div>
		</div>
	</form>
	<script type="text/javascript">
		var baseUrl = "";
		var post_url = "<?php echo U('index');?>";
	</script>
	<script type="text/javascript" src="/Public/layuicms/layui/layui.js"></script>
	<script type="text/javascript" src="/Public/admin/js/lib/public.js"></script>
	<script type="text/javascript" src="/Public/admin/js/system_index.js"></script>
</body>

</html>