<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>角色分组</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="/Public/layuicms/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/Public/layuicms/css/public.css" media="all" />
</head>
<body class="childrenBody">
<form class="layui-form">
	<blockquote class="layui-elem-quote quoteBox">
		<form class="layui-form">
			<div class="layui-inline">
				<a class="layui-btn layui-btn-normal add-manage-btn">添加</a>
			</div>
		</form>
	</blockquote>
	<table id="role-list" lay-filter="role-list"></table>	
</form>
<script type="text/javascript">
	var dataUrl = "<?php echo U('index');?>";
	var delUrl  = "<?php echo U('del');?>";
	var addUrl  = "<?php echo U('add');?>";
	var updUrl  = "<?php echo U('upd');?>";
	var authUrl = "<?php echo U('auth');?>";
</script>
<script type="text/javascript" src="/Public/layuicms/layui/layui.js"></script>
<script type="text/javascript" src="/Public/admin/js/lib/public.js"></script>
<script type="text/javascript" src="/Public/admin/js/role_index.js"></script>
</body>
</html>