<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>修改密码</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="/Public/layuicms/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/Public/layuicms/css/public.css" media="all" />
	<style>
		.border-box {
			border: 1px solid #eee;
			padding: 10px 10px 10px 0
		}

		.disabled {
			background: #eee
		}
	</style>
</head>

<body class="childrenBody">
	<form class="layui-form layui-row layui-col-space10" id="form">
		<div class="layui-col-md9 layui-col-xs12 border-box">

			<div class="layui-form-item magt3">
				<label class="layui-form-label">原密码</label>
				<div class="layui-input-block">
					<input type="password" class="layui-input" lay-verify="required" name="oldpwd" placeholder="请输入原密码">
				</div>
			</div>
			<div class="layui-form-item magt3">
				<label class="layui-form-label">新密码</label>
				<div class="layui-input-block">
					<input type="password" class="layui-input" name="newpwd" lay-verify="required" placeholder="请输入新密码">
				</div>
			</div>
			<div class="layui-form-item magt3">
				<label class="layui-form-label">确认密码</label>
				<div class="layui-input-block">
					<input type="password" class="layui-input" lay-verify="required" name="okpwd" placeholder="请再输入一遍">
				</div>
			</div>
			<hr class="layui-bg-gray" />
			<div class="layui-right">
				<a class="layui-btn layui-btn-sm" lay-filter="upd-btn" lay-submit><i
						class="layui-icon">&#xe609;</i>提交</a>
			</div>
		</div>
	</form>
	<script type="text/javascript">
		var upd_url = "<?php echo U('updpwd');?>";
	</script>
	<script type="text/javascript" src="/Public/layuicms/layui/layui.js"></script>
	<script type="text/javascript" src="/Public/admin/js/lib/public.js"></script>
	<script>
		layui.use(['form', 'layer'], function () {
			var form = layui.form,
				layer = parent.layer === undefined ? layui.layer : top.layer,
				laypage = layui.laypage,
				$ = layui.jquery;

			//表单提交
			form.on("submit(upd-btn)", function (data) {
				//弹出loading
				var index = top.layer.msg('数据提交中，请稍候', { icon: 16, time: false, shade: 0.8 });
				// 实际使用时的提交信息
				setTimeout(function () {
					$.ajax({
						url: upd_url,
						data: $('#form').serialize(),
						type: "POST",
						dataType: "json",
						success: function (res) {
							if (res.code == 0) {
								layer.msg(res.message);
							} else {
								layer.msg(res.message, { icon: 5 });
							}
						}
					});
				}, 500);
			})

		})
	</script>
</body>

</html>