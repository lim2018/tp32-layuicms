<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>首页--layui后台管理模板 2.0</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="/Public/layuicms/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/Public/layuicms/css/public.css" media="all" />
</head>
<body class="childrenBody">
	<blockquote class="layui-elem-quote layui-bg-green">
		<div id="nowTime">亲爱的<strong><?php echo session("manage")["username"];?></strong>，下午好！ 欢迎使用<?php echo ($sys_info["site_name"]); ?>。 </div>
	</blockquote>
	
	<div class="layui-row layui-col-space10">
		<div class="layui-col-lg6 layui-col-md12">
			<blockquote class="layui-elem-quote title">系统基本参数</blockquote>
			<table class="layui-table magt0">
				<colgroup>
					<col width="150">
					<col>
				</colgroup>
				<tbody>
				<tr>
					<td>网站域名</td>
					<td><?php echo ($_SERVER['HTTP_HOST']); ?></td>
				</tr>
				<tr>
					<td>网站目录</td>
					<td><?php echo ($_SERVER['DOCUMENT_ROOT']); ?></td>
				</tr>
				<tr>
					<td>服务器操作系统</td>
					<td><?php echo php_uname('s');?></td>
				</tr>
				<tr>
					<td>服务器端口</td>
					<td><?php echo ($_SERVER['SERVER_PORT']); ?></td>
				</tr>
				<tr>
					<td>PHP版本</td>
					<td><?php echo phpversion();?></td>
				</tr>
				<tr>
					<td>最大上传限制</td>
					<td><?php echo ini_get('upload_max_filesize');?></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<script type="text/javascript">
		var public = "/Public";
	</script>
	<script type="text/javascript" src="/Public/layuicms/layui/layui.js"></script>
</body>
</html>