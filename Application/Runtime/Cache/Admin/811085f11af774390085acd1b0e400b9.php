<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>个人资料</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="/Public/layuicms/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/Public/layuicms/css/public.css" media="all" />
</head>
<body class="childrenBody">
	<div class="layui-row layui-col-space10">
		<div class="layui-col-lg6 layui-col-md9">
			<blockquote class="layui-elem-quote title">个人资料</blockquote>
			<table class="layui-table magt0">
				<colgroup>
					<col width="150">
					<col>
				</colgroup>
				<tbody>
				<tr>
					<td>ID</td>
					<td><?php echo session("manage")["manage_id"];?></td>
				</tr>
				<tr>
					<td>用户名</td>
					<td><?php echo session("manage")["username"];?></td>
				</tr>
				<tr>
					<td>邮箱</td>
					<td><?php echo session("manage")["email"];?></td>
				</tr>
				<tr>
					<td>性别</td>
					<td>
						<?php $user_sex = session("manage")["sex"]; ?>
						<?php if(($user_sex) == "0"): ?>女<?php endif; ?>
						<?php if(($user_sex) == "1"): ?>男<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td>加入时间</td>
					<td>
						<?php $user_add_time = date('Y-m-d H:i:s', session("manage")["user_add_time"]); ?>
						<?php echo ($user_add_time); ?>
					</td>
				</tr>
				<tr>
					<td>描述说明</td>
					<td><?php echo session("manage")["description"];?></td>
				</tr>
				<tr>
					<td>角色权限</td>
					<td>
						<?php echo ($role["rname"]); ?>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<script type="text/javascript">
		var public = "/Public";
	</script>
	<script type="text/javascript" src="/Public/layuicms/layui/layui.js"></script>
</body>
</html>