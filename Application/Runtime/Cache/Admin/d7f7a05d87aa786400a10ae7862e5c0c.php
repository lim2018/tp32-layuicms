<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>编辑用户</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="/Public/layuicms/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="/Public/layuicms/css/public.css" media="all" />
	<style>
		.border-box {border: 1px solid #eee;padding: 10px 10px 10px 0}
		.disabled {background: #eee}
	</style>
</head>
<body class="childrenBody">
<form class="layui-form layui-row layui-col-space10" id="form">
	<div class="layui-col-md6 layui-col-xs12 border-box">
	
		<div class="layui-form-item magt3">
			<label class="layui-form-label">用户名称</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input username disabled" lay-verify="username" name="username"
					value="<?php echo ($data["username"]); ?>" placeholder="请输入文章标题" readonly>
			</div>
		</div>
		<div class="layui-form-item magt3">
			<label class="layui-form-label">邮箱</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input email" lay-verify="email" name="email"
					value="<?php echo ($data["email"]); ?>" placeholder="请输入文章标题">
			</div>
		</div>
		<div class="layui-form-item magt3">
			<label class="layui-form-label">性别</label>
			<div class="layui-input-block">
				<input type="radio" name="sex" value="0" title="女" lay-skin="primary"  <?php if(($data["sex"]) == "0"): ?>checked<?php endif; ?>/>
				<input type="radio" name="sex" value="1" title="男" lay-skin="primary"  <?php if(($data["sex"]) == "1"): ?>checked<?php endif; ?>/>
			</div>
		</div>
		<div class="layui-form-item releaseDate">
			<label class="layui-form-label">创建时间</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input disabled" name="user_add_time" lay-verify="required"
					value="<?php echo date('Y-m-d H:i:s',$data['user_add_time']);?>" id="user_add_time" placeholder="请选择日期和时间" readonly />
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">状态</label>
			<div class="layui-input-block status">
				<select name="status" lay-verify="required">
					<option value="0" <?php if(($data["status"]) == "0"): ?>selected<?php endif; ?>>正常</option>
					<option value="1" <?php if(($data["status"]) == "1"): ?>selected<?php endif; ?>>封禁</option>
				</select>
			</div>
		</div>
		
		<div class="layui-form-item openness">
			<label class="layui-form-label">角色</label>
			<div class="layui-input-block role_id">
				<select name="role_id" lay-verify="required">
					<?php if(is_array($role_list)): $i = 0; $__LIST__ = $role_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["rid"]); ?>" 
						<?php if(($vo["rid"]) == $data["role_id"]): ?>selected<?php endif; ?>><?php echo ($vo["rname"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
				</select>
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">描述备注</label>
			<div class="layui-input-block">
				<textarea name="description" placeholder="请输入描述备注" class="layui-textarea description"><?php echo ($data["description"]); ?></textarea>
			</div>
		</div>
		<hr class="layui-bg-gray" />
		<div class="layui-right">
			<input type="hidden" name="manage_id" value="<?php echo ($data["manage_id"]); ?>" />
			<a class="layui-btn layui-btn-sm" lay-filter="upd-btn" lay-submit><i class="layui-icon">&#xe609;</i>提交</a>
		</div>
	</div>
</form>
<script type="text/javascript">
	var uploadImageUrl = "<?php echo U('Upload/uploadImage');?>";
	var baseUrl = "";
	var updUrl = "<?php echo U('upd');?>";
</script>
<script type="text/javascript" src="/Public/layuicms/layui/layui.js"></script>
<script type="text/javascript" src="/Public/admin/js/lib/public.js"></script>
<script type="text/javascript" src="/Public/admin/js/manage_form.js"></script>
</body>
</html>